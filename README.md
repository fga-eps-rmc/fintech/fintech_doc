## Sobre

Repositório de documentação do projeto Fintech. Este espaço concentra todos os documentos produzidos ao longo do processo de desenvolvimento do projeto, desde documentos técnicos até atas de reuniões semanais.

[GitLab Pages](https://fintech-doc-fga-eps-rmc-fintech-7c468b31f1296a85e3d478ec3c6978a.gitlab.io/)


## Ambientes

- 

## Integrantes

### 2024-1

| Nome | Matricula | Papel | Gitlab | Email |
|-----------|------|--------|--------|------|
| Ailton Aires Amado | 180011600 | Dev | ailtonaires | 180011600@aluno.unb.br |
| Cibele Freitas Goudinho | 180099353 | Dev | CibeleG | cibelegoudinho13@gmail.com |
| Eurico Menezes de Abreu Neto | 200017519 | Dev  | EuricoAbreu | 200017519@aluno.unb.br |
| Guilherme Daniel Fernandes da Silva | 180018019 | Product Owner | guilhermedfs14 | 180018019@aluno.unb.br |
| Joao Henrique Marques Calzavara | 200067923 | Dev | joao-henrique10 | jhcalzavara@hotmail.com |
| João Vitor de Souza Durso | 180123459 | Dev | jvsdurso | jvsdurso@gmail.com |
| João Vitor Ferreira Alves | 160127912 | Tester | vitorAlves7 | 160127912@aluno.unb.br |
| Julia Farias Sousa | 180103792 | Designer | julisous | 180103792@aluno.unb.br |
| Kayro César Silva Machado | 170107426 | Dev | kayrocesar | kayro.cesar.kc@gmail.com |
| Kevin Luis Apolinario Batista | 180042386 | Dev | kevinluis | k.luis360@gmail.com |
| Lude Yuri de Castro Ribeiro | 150137770 | Tech Lead | luderibeiro | 150137770@aluno.unb.br |
| Mateus Moreira Lima | 180024868 | Scrum Master | mateus_lm | 180024868@aluno.unb.br |
| Paulo Gonçalves Lima | 190047968 | Dev | PauloAbiAcl | 170122549@aluno.unb.br |
| Paulo Vitor Silva Abi Acl	 | 190047968 | Dev | pauloAbiACL | 190047968@aluno.unb.br |
| Victor Buendia Cruz de Alvim | 190020601 | Tester | Victor-Buendia | 190020601@aluno.unb.br |
| Victor Rayan Adriano Ferreira | 190044390 | Tester | victor-rayan | 190044390@aluno.unb.br |
| Yuri Alves Bacarias | 180078640 | Designer | yurialves5 | 180078640@aluno.unb.br |

